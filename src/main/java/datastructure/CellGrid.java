package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                grid[row][col] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (isValidCoordinate(row, column)) {
            this.grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException("Coordinate doesnt exist.");
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (isValidCoordinate(row, column)) {
            return this.grid[row][column];
        } else {
            throw new IndexOutOfBoundsException("Coordinate doesnt exist.");
        }
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                gridCopy.set(row, col, get(row, col));
            }
        }
        return gridCopy;
    }

    public boolean isValidCoordinate(int row, int column) {
        if (row < 0 || column < 0 || row > this.rows || column > this.columns) {
            return false;
        }
        return true;
    }

}
